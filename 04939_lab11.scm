(define ADD(lambda (a b)         
		(SUCC a (SUCC b 0)) ))
		 
(define SUB(lambda (a b)         
		(PRED b a) ))
		
(define AND(lambda (M N)         
		 (N (M TRUE FALSE) FALSE)))
		 
(define OR(lambda (M N)         
		 (N TRUE (M TRUE FALSE))))

(define NOT(lambda (M)         
		 (M FALSE TRUE)))
		 
(define TRUE(lambda (a b)         
		 a ))
		 
(define FALSE(lambda (a b)         
		 b ))

(define LEQ(lambda (a b)       
		(ISZERO (SUB a b)) ))
(define LQ(lambda (a b)       
		(AND (ISZERO (SUB a b)) (NOT ( iszero (SUB b a) )))))
(define GEQ(lambda (a b)       
		(ISZERO (SUB b a)) ))
		 
(define (SUCC n z)
	  (define (iter n z)
	    (if (zero? n)
	        z
	        (iter (- n 1) (+ z 1))))
	  (iter n z))
	  
(define (PRED n z)
	  (define (iter n z)
		(if (zero? z)
			z
			(if (zero? n)
				z
				(iter (- n 1) (- z 1)))))
	  (iter n z))
	  
(define (ISZERO n)
	  (if (zero? n)
		TRUE
		FALSE
	  ))

(define (IFTHENELSE p q r)
	  ( p q r) )
	  
	  
(define (FOR n)
		
		(define (iter n sum i)
	    (if (zero? n)
			sum
	        (iter (SUB n 1) (ADD sum i) (ADD i 1))))
		(iter n sum 0)
		)
(define sum 0)
(define (aCompletelyUselessFunctionCreatedOnlyToMakeYourLifeMiserable m n)
		
		(IFTHENELSE (OR (LQ m n) (ISZERO m) )
		(FOR n)
		(ADD n m))
		)